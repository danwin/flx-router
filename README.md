# flx-router

> Hash-based router for browser apps. Plain JS. No dependencies. 

## Install

Install with [npm](https://www.npmjs.com/)

```sh
$ npm i flx-router --save-dev
```

## Usage

```js
var BrickRouterFactory = require('flx-router');
BrickRouterFactory({
    routes: 'route1|route2|route3',
    defaultRoute: 'route1', /* route for empty hash */
    errorRoute: 'route1', /* handle your "404":) */
    processor: function(route){
        document.body.setAttribute('data-route-value', route);
        // use some switchable selector to apply further css-based visibility, you can also use classes.
    }
});
// Then you can add simple css rules: #mypage1, #mypage2, #mypage3 {display: none}; [data-route-value=route1] #mypage1 {display: block}
```

## Running tests

Install dev dependencies:

```sh
$ npm i -d && npm test
```

## Contributing

Pull requests and stars are always welcome. For bugs and feature requests, [please create an issue](https://gitlab.com/danwin/flx-router/issues)

## Author

**Danwin**

* [github/](https://gitlab.com/danwin)

## License

Copyright © 2018 [Danwin](#Danwin)
Licensed under the ISC license.

***

