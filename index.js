function _extend(obj1, obj2) {
    for (var name in obj2) {
        if (obj2.hasOwnProperty(name)) {
            obj1[name] = obj2[name]
        }
    }
    return obj1;
}

function createMap(keysList) {
    var mapping = {}, 
        keysArray = (typeof keysList === 'string') ? keysList.split('|') : keysList;
    for (var i = 0, key; i < keysArray.length; i++) {
        key = keysArray[i];
        mapping[key] = true;
    }
    return mapping;
}

function defaultRenderer(route){
    document.body.setAttribute('data-route-selector', route)
}

const defaultOptions = {
    // List of routes identifiers: array or string (delimited with pipes "|")
    routes: createMap([]),
    // Callback to update UI for new route
    processor: defaultRenderer,
    // identifier for route which not exists in routesList
    errorRoute: 404,
    // default route identitier for empty hash
    defaultRoute: 'index'
};

function RouterFactory(options){
    'use strict'

    var 
    o = options || {},
    self = _extend(defaultOptions, o);

    self.routesMap = createMap(self.routes);

    self.setRoute = function(route) {
        route = (/^#/.test(route)) ? route : '#/'+route;
        if(history.pushState) {
            history.pushState(null, null, route);
        }
        else {
            window.location.hash = route;
        }
    }

    function handleHash() {
        var route = window.location.hash || '';
        route = route.replace(/^#\/*/, '');
        console.log('#hash is:', route);
        if (route === '') return self.processor(self.defaultRoute);
        if (route in self.routesMap) return self.processor(route);
        self.processor(self.errorRoute);
    }

    window.addEventListener('DOMContentLoaded', handleHash);
    window.addEventListener('hashchange', handleHash);

    return self;
}

module.exports = RouterFactory;